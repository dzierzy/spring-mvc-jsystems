package pl.jsystems.spring.librarian.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import pl.jsystems.spring.librarian.dao.BooksDAO;
import pl.jsystems.spring.librarian.model.Book;
import pl.jsystems.spring.librarian.model.Publisher;
import pl.jsystems.spring.librarian.service.api.BrowsingService;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class SimpleLibrarianService implements BrowsingService {

    Logger logger = Logger.getLogger(SimpleLibrarianService.class.getName());

    @Autowired
    private BooksDAO bDao;

    @Autowired
    PlatformTransactionManager tm;

    public List<Publisher> getPublishers() {
        return bDao.getAllPublishers();
    }

    public Publisher getPublisher(Long id){
        return bDao.getPublisherById(id);
    }



    public List<Book> getBooksForPublisher(Long rId) {
        Publisher r = getPublisher(rId);
        return bDao.getBooksByPublisher(r);
    }

    public Book getBookById(Long mId) {
        return bDao.getBookById(mId);
    }

    public Publisher addPublisher(Publisher p) {
        throw new UnsupportedOperationException("not implemented here");
    }



    // REQUIRED -> create new if missing or join to existing one
    // REQUIRES_NEW -> create new
    // MANDATORY -> musi byc utworzona
    // SUPPORTS -> join existing one or execute non-transactional
    // NEVER -> always non-transactional
    // NOT_SUPPORTED -> throw exception if transaction is open
    // NESTED -> ???

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Book addBook(Publisher p, Book b) {

        //TransactionStatus ts = tm.getTransaction(new DefaultTransactionDefinition());

        try {
            b.setPublisher(bDao.getPublisherById(p.getId()));

            b = bDao.addBook(b);
            if(b.getPrice()==0) {
                bDao.addPublisher(new Publisher());
            }

            //tm.commit(ts);

        }catch (RuntimeException e){
            //tm.rollback(ts);
            throw e;
        }



        return b;
    }

}
