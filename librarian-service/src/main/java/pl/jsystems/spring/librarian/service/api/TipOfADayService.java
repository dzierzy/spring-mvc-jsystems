package pl.jsystems.spring.librarian.service.api;

public interface TipOfADayService {

	String getNextTip();
}
