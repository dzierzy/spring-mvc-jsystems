package pl.jsystems.spring.librarian.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jsystems.spring.librarian.service.api.TipOfADayService;

import java.util.List;
import java.util.Random;

@Service
public class TipServiceImpl implements TipOfADayService {

    @Autowired
    List<String> tips;


    @Override
    public String getNextTip() {
        int randomIndex = new Random().nextInt(tips.size());
        return tips.get(randomIndex);
    }
}
