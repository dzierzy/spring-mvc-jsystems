package pl.jsystems.spring.librarian.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jsystems.spring.librarian.model.Book;
import pl.jsystems.spring.librarian.model.Order;
import pl.jsystems.spring.librarian.service.api.OrderService;


import java.io.Serializable;
import java.util.Set;
import java.util.logging.Logger;



@Service
public class SessionOrderService implements OrderService, Serializable {

    Logger logger = Logger.getLogger(SessionOrderService.class.getName());

    @Autowired
    private Order o;

    public void add(Book m) {
        o.addMeal(m);
    }

    public void remove(Book m) {
        o.removeMeal(m);
    }

    public Set<Book> getOrderedMeals() {
        return o.getBooks();
    }


    public void doit() {
       logger.info("order confirmation: " + o);
       o.reset();
    }

    public void clear() {
       o.reset();
    }
}
