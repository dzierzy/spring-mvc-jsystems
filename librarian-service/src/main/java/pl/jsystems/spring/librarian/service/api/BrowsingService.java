package pl.jsystems.spring.librarian.service.api;

import pl.jsystems.spring.librarian.model.Book;
import pl.jsystems.spring.librarian.model.Publisher;

import java.util.List;

/**
 * Created by xdzm on 2015-10-12.
 */
public interface BrowsingService {

    List<Publisher> getPublishers();

    public Publisher getPublisher(Long id);

    List<Book> getBooksForPublisher(Long rId);

    Book getBookById(Long mId);

    Publisher addPublisher(Publisher r);

    Book addBook(Publisher r, Book m);
}
