package pl.jsystems.spring.librarian.service.api;

import pl.jsystems.spring.librarian.model.Book;

import java.util.Set;

/**
 * Created by xdzm on 2015-10-14.
 */
public interface OrderService {

    void add(Book m);

    void remove(Book m);

    Set<Book> getOrderedMeals();

    void doit();

    void clear();

}
