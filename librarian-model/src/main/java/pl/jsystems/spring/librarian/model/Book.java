package pl.jsystems.spring.librarian.model;


import javax.persistence.*;
import java.io.Serializable;


@NamedQueries(
        {
                @NamedQuery(
                        name = "Book.SELECT_BY_PUBLISHER",
                        query = "select b from Book b where b.publisher=:p order by b.title"
                )
        }
)

@Entity
public class Book implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String author;

    private String cover;

    private int price;

    @ManyToOne
    //@JoinColumn(name = "PUBLISHER_ID", nullable = false)
    private Publisher publisher; // PUBLISHER_ID

    @Transient
    private Long publisherId;

    public Book(Long id, String title, String author, String cover, int price, Publisher servedIn) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.cover = cover;
        this.price = price;
        this.publisher = servedIn;
    }

    public Book(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }




    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", cover='" + cover + '\'' +
                ", price=" + price +
                ", publisher=" + publisher +
                ", publisherId=" + publisherId +
                '}';
    }
}
