package pl.jsystems.spring.librarian.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@NamedQueries(
        {
                @NamedQuery(
                        name="Publisher.SELECT_ALL",
                        query = "select p from Publisher p"
                )
        }

)


@Entity
//@Table("PUBLISHER")
public class Publisher implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="NAME", length = 100, nullable = false)
    private String name;

    private String logoImage;

    @OneToMany(mappedBy = "publisher")
    private transient List<Book> books = new ArrayList<Book>();


    public Publisher(Long id, String name, String logoImage) {
        this.id = id;
        this.name = name;
        this.logoImage = logoImage;
    }

    public Publisher() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoImage() {
        return logoImage;
    }

    public void setLogoImage(String logoImage) {
        this.logoImage = logoImage;
    }


    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    @Override
    public String toString() {
        return "Publisher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", logoImage='" + logoImage + '\'' +
                '}';
    }
}
