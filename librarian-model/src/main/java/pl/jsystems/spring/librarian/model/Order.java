package pl.jsystems.spring.librarian.model;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;



@Component
@Scope(scopeName = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Order implements Serializable {

    private Long id;

    private Set<Book> books = new HashSet<Book>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    public void addMeal(Book m) {
        books.add(m);
    }

    public void removeMeal(Book m) {
        books.remove(m);
    }

    public void reset() {
        books.clear();
    }

    @Override
    public String toString() {
        return "Order{" +
                "books=" + books +
                '}';
    }
}
