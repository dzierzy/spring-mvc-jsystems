package pl.jsystems.spring.librarian.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class MyConfiguration {

    @Bean
    List<String> getTips(){
        return Arrays.asList(
                "Kto czyta nie bladzi",
                "Ucz sie ucz bo nauka to potegi klucz",
                "Burn after reading..."
        );
    }
}
