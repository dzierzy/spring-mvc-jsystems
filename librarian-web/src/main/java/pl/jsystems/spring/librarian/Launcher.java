package pl.jsystems.spring.librarian;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.jsystems.spring.librarian.service.api.BrowsingService;

public class Launcher {

    public static void main(String[] args) {
        System.out.println("Launcher.main");

        ApplicationContext context = new AnnotationConfigApplicationContext(LibrarianConfiguration.class);
        BrowsingService bs = context.getBean(BrowsingService.class);

        bs.getPublishers().forEach(p-> System.out.println("publisher: " + p));
    }
}
