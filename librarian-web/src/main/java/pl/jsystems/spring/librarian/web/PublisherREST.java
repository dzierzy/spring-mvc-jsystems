package pl.jsystems.spring.librarian.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.jsystems.spring.librarian.model.Publisher;
import pl.jsystems.spring.librarian.service.api.BrowsingService;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class PublisherREST {

    Logger logger = Logger.getLogger(PublisherREST.class.getName());

    @Autowired
    BrowsingService browsingService;

    @GetMapping("/publishersData")
    List<Publisher> getAllPublishers(){
        logger.info("about to fetch publishers list data.");

        List<Publisher> publishers = browsingService.getPublishers();
        logger.info("there is " + publishers.size() + " publishers");
        return publishers;
    }
}
