package pl.jsystems.spring.librarian.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

public class TimeAccessInterceptor implements HandlerInterceptor {

    private int opening;

    private int closing;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int now = LocalTime.now().getHour();

        if(now >= opening && now < closing){
            return true;
        } else {
            response.sendRedirect("https://www.tenstickers.pl/naklejki-dekoracyjne/img/large/naklejka-napis-closed-1833.jpg");
            return false;
        }
    }

    public int getOpening() {
        return opening;
    }

    public void setOpening(int opening) {
        this.opening = opening;
    }

    public int getClosing() {
        return closing;
    }

    public void setClosing(int closing) {
        this.closing = closing;
    }
}
