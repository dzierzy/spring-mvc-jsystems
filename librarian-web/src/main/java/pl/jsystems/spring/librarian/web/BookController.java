package pl.jsystems.spring.librarian.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.jsystems.spring.librarian.config.Filter;
import pl.jsystems.spring.librarian.model.Book;
import pl.jsystems.spring.librarian.model.Publisher;
import pl.jsystems.spring.librarian.service.api.BrowsingService;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Controller
@RequestMapping("/books")
@Filter
@SessionAttributes("publisherContext")
public class BookController {

    Logger logger = Logger.getLogger(BookController.class.getName());

    @Autowired
    BrowsingService browsingService;

    @Autowired
    BookValidator bookValidator;

    @InitBinder("book")
    void initBinding(WebDataBinder binder){
        binder.setValidator(bookValidator);
    }

    //@RequestMapping(method = RequestMethod.GET)
    @GetMapping
    public String getBooksByPublisher(
            Model model,
            @RequestParam("publisherId") Long publisherId,
            @RequestHeader(value="User-Agent", required = false) String userAgent,
            @RequestHeader HttpHeaders headers,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId
    ){
        logger.info("obtaining books of publisher " + publisherId );
        logger.info("client [" + sessionId + "] connected using " + userAgent);

        for(Map.Entry<String, List<String>> entry : headers.entrySet()){
            StringBuilder sb = new StringBuilder();
            for(String value : entry.getValue()){
                sb.append(value + ",");
            }
            logger.info("header key=[" + entry.getKey() + "] value=[" + sb.toString() + "]");
        }

        List<Book> books = browsingService.getBooksForPublisher(publisherId);
        Publisher p = browsingService.getPublisher(publisherId);

        model.addAttribute("books", books);
        model.addAttribute("title", "Books of publisher " + p.getName());
        model.addAttribute("publisher", p);


        return "books";
    }

    @GetMapping("/add")
    public String addBookPrepare(Model model, @RequestParam("publisherId") Long publisherId){
        logger.info("preparing book form");

        Publisher p = browsingService.getPublisher(publisherId);
        Book b = new Book();
        model.addAttribute("book", b);
        model.addAttribute("publisherContext", p);

        return "addBook";
    }

    @PostMapping("/add")
    public String addBook(
            @ModelAttribute("book") @Validated Book book,
            BindingResult br,
            @ModelAttribute("publisherContext") Publisher p
    ){

       //throw new UnsupportedOperationException("fake problem");

        logger.info("about to add new book for publisher " + p);

        if(br.hasErrors()){
            return "addBook";
        }
        browsingService.addBook(p, book);

        return "redirect:/books?publisherId=" + p.getId();
    }

}
