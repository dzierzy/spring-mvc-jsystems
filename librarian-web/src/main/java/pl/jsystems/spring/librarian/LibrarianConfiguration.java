package pl.jsystems.spring.librarian;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("pl.jsystems.spring.librarian")
public class LibrarianConfiguration {
}
