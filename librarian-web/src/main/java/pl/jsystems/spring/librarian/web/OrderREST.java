package pl.jsystems.spring.librarian.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.jsystems.spring.librarian.model.Book;
import pl.jsystems.spring.librarian.service.api.BrowsingService;
import pl.jsystems.spring.librarian.service.api.OrderService;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;


// Spring REST vs JAX-RS

@RestController
@RequestMapping("/orderitems")
public class OrderREST {

    Logger logger = Logger.getLogger(OrderREST.class.getName());

    @Autowired
    BrowsingService browsingService;

    @Autowired
    OrderService orderService;


    @PostMapping("/{id}")
    void addBook(@PathVariable("id") Long id){
        logger.info("adding book " + id + " to a cart");

        Book book = browsingService.getBookById(id);
        orderService.add(book);
    }

    @DeleteMapping("/{id}")
    void deleteBook(@PathVariable("id") Long id){

        logger.info("removing book " + id + " from a cart");

        Book book = browsingService.getBookById(id);
        orderService.remove(book);
    }

    @DeleteMapping
    void deleteAll(){
        logger.info("clearing a cart");
        orderService.clear();
    }


    @GetMapping
    Set<Book> getBooks(){
        logger.info("fetching books from cart");
        return orderService.getOrderedMeals();
    }

}
