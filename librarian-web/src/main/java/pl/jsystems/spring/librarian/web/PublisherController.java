package pl.jsystems.spring.librarian.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.jsystems.spring.librarian.config.Filter;
import pl.jsystems.spring.librarian.model.Publisher;
import pl.jsystems.spring.librarian.service.api.BrowsingService;
import pl.jsystems.spring.librarian.service.api.TipOfADayService;

import java.util.List;
import java.util.logging.Logger;

@Controller
@Filter
public class PublisherController {

    Logger logger = Logger.getLogger(PublisherController.class.getName());

    @Autowired
    BrowsingService browsingService;

    @RequestMapping(value="/publishers", method = RequestMethod.GET)
    public String getAllPublishers(Model model){
        logger.info("about to fetch publishers list.");

        List<Publisher> publishers = browsingService.getPublishers();

        model.addAttribute("publishers", publishers);
        model.addAttribute("title", "Publishers list");

        return "publishers";
    }








}
