package pl.jsystems.spring.librarian.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.jsystems.spring.librarian.model.Book;

@Component
public class BookValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Book.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Book book = (Book) o;

        if(book.getPrice()<=0){
            errors.rejectValue("price", "errors.book.price.negative");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "errors.book.title.empty");


    }
}
