package pl.jsystems.spring.librarian.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import pl.jsystems.spring.librarian.service.api.TipOfADayService;
import pl.jsystems.spring.librarian.web.PublisherController;

@ControllerAdvice(
        /*basePackages = "pl.jsystems.spring.librarian.web",
        basePackageClasses = PublisherController.class,
        assignableTypes = PublisherController.class,*/
        annotations = Filter.class)
public class MyControllerAdvice {

    @Autowired
    TipOfADayService tipOfADayService;

    @ModelAttribute
    private void addTip(Model model){
        model.addAttribute("tip", tipOfADayService.getNextTip());
    }


    @ExceptionHandler(UnsupportedOperationException.class)
    String handleException(UnsupportedOperationException e, Model model){
        model.addAttribute("error", e.getMessage());

        return "error";
    }

}
