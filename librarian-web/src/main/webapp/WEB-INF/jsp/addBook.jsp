<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



<jsp:include page="./header.jsp" flush="true"/>

        <form:form action="add" method="post" modelAttribute="book">
            <table>
                <tr>
                    <td>
                        <spring:message code="label.book.title"/>: <form:input path="title"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <spring:message code="label.book.author"/>: <form:input path="author"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <spring:message code="label.book.price"/>: <form:input path="price"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <spring:message code="label.book.cover"/>: <form:input path="cover"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="<spring:message code="label.book.submit"/>"/>
                        <form:errors path="*"/>
                    </td>
                </tr>

            </table>
        </form:form>

<jsp:include page="./footer.jsp" flush="true"/>
