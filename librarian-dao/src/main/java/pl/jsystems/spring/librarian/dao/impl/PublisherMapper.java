package pl.jsystems.spring.librarian.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import pl.jsystems.spring.librarian.model.Publisher;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PublisherMapper implements RowMapper<Publisher> {
    @Override
    public Publisher mapRow(ResultSet rs, int i) throws SQLException {
        Publisher p = new Publisher();
        p.setId(rs.getLong("publisher_id"));
        p.setName(rs.getString("publisher_name"));
        p.setLogoImage(rs.getString("publisher_logo"));
        return p;
    }
}
