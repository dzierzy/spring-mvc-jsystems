package pl.jsystems.spring.librarian.dao.impl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.jsystems.spring.librarian.dao.BooksDAO;
import pl.jsystems.spring.librarian.model.Book;
import pl.jsystems.spring.librarian.model.Publisher;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Primary
public class JpaBooksDAO implements BooksDAO {

    @PersistenceContext(name="librarianUnit")
    private EntityManager em;

    // HQL
    // JPQL
    @Override
    public List<Publisher> getAllPublishers() {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Publisher> cq = cb.createQuery(Publisher.class);
        Root<Publisher> root = cq.from(Publisher.class);
        cq.select(root);

        Query query =  em.createQuery(cq);
        return query.getResultList();

        //return em.createNamedQuery("Publisher.SELECT_ALL").getResultList();
    }

    @Override
    public Publisher getPublisherById(Long id) {
        return em.find(Publisher.class, id);
    }

    @Override
    public List<Book> getBooksByPublisher(Publisher p) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        Root<Book> root = cq.from(Book.class);
        cq.select(root);

        cq.where(cb.equal( root.get("publisher"),  p ));

        Query query = em.createQuery(cq);



        return query.getResultList();

        /*return em
                //.createQuery("select b from Book b where b.publisher=:p")
                .createNamedQuery("Book.SELECT_BY_PUBLISHER")
                .setParameter("p", p)
                //.setMaxResults(10)
                //.setFirstResult(30)
                .getResultList();*/
    }

    @Override
    public Book getBookById(Long id) {
        // return em.find(Book.class, id);

        return  (Book) em
                .createQuery("select b from Book b where b.id=:id")
                .setParameter("id", id)
                .getSingleResult();

    }

    @Override
    public Publisher addPublisher(Publisher p) {
        em.persist(p);
        return p;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Book addBook(Book b) {
         em.persist(b);
         return b;
    }
}
